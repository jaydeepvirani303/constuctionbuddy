//
//  AIEnums.swift
//  Swift3CodeStructure
//
//  Created by Ravi Alagiya on 25/11/2016.
//  Copyright © 2016 Ravi Alagiya. All rights reserved.
//

import Foundation
import UIKit

//MARK:- AIEdge
enum AIEdge:Int {
	case
	top,
	left,
	bottom,
	right,
	top_Left,
	top_Right,
	bottom_Left,
	bottom_Right,
	all,
	none
}

enum socialStatus {
    case facebook
    case apple
    case normal
    case instagram
}

enum serviceStatus {
    case add
    case edit
}

enum dateFormatter:String {
    case dateFormate1 = "hh:mm a"
    case dateFormate2 = "hh:mm:ss"
    case dateFormate3 = "HH:mm:ss"
    case dateFormate4 = "dd MMM yyyy"
    case dateFormate5 = "dd MMM yyyy hh:mm a"
    case dateFormate6 = "yyyy-MM-dd"
    case dateFormate7 = "yyyy-MM-dd HH:mm:ss"
}

enum gender:String {
    case male = "male"
    case female = "female"
    case other = "other"
}

enum checkoutParent {
    case home
    case cart
}

enum selectAppointment:String {
    case past = "0"
    case upcoming = "1"
}

