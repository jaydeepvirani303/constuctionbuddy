//
//  AIServiceConstants.swift
//  Swift3CodeStructure
//
//  Created by Ravi Alagiya on 25/11/2016.
//  Copyright © 2016 Ravi Alagiya. All rights reserved.
//

import Foundation

//MARK:- BASE URL

let URL_BASE                    = "http://138.68.251.145/api/barber/dev"
let URL_USER_PROFILE_PIC        = "http://138.68.251.145/api/barber/app_images/user_profile/"
let URL_SERVICE_PIC             = "http://138.68.251.145/api/barber/app_images/service_images/"
let URL_USER_PIC                = "http://138.68.251.145/api/barber/app_images/user_pictures/"

// Postfix URL

let URL_SIGN_UP = getFullUrl("/Service.php?Service=userRegistration")
let URL_LOGIN = getFullUrl("/Service.php?Service=userlogin")
let URL_FORGOT_PASSWORD = getFullUrl("/Service.php?Service=forgotPassword")
let URL_VERIFY_CHANGE_PASSWORD = getFullUrl("/Service.php?Service=changePasswordWithVerifyCode")
let URL_CHECK_SOCIAL_LOGIN = getFullUrl("/Service.php?Service=checkSocialLogin")
let URL_GET_SERVICE_LIST = getFullUrl("/Service.php?Service=getServicetList")
let URL_GET_SERVICE_CATEGORY_LIST = getFullUrl("/Service.php?Service=getServiceCategory")
let URL_ADD_SERVICE_LIST = getFullUrl("/Service.php?Service=addService")
let URL_EDIT_SERVICE_LIST = getFullUrl("/Service.php?Service=editService")
let URL_DELETE_SERVICE_LIST = getFullUrl("/Service.php?Service=deleteService")
let URL_GET_USER_DETAIL_FROM_ID = getFullUrl("/Service.php?Service=getUserDetailsFromId")
let URL_ADD_USER_PICTURE = getFullUrl("/Service.php?Service=addUserPictures")
let URL_DELETE_USER_PICTURE = getFullUrl("/Service.php?Service=deleteUserPictures")
let URL_UPDATE_WEBSITE_ABOUT = getFullUrl("/Service.php?Service=updateWebsiteAndAbout")
let URL_UPDATE_USER_DETAIL = getFullUrl("/Service.php?Service=updateUserDetails")
let URL_CHANGE_PASSWORD = getFullUrl("/Service.php?Service=changePassword")
let URL_ADD_AVAILABILITY = getFullUrl("/Service.php?Service=addAvailability")
let URL_ADD_FACEBOOK_ID = getFullUrl("/Service.php?Service=addFacebookId")
let URL_REMOVE_FACEBOOK_ID = getFullUrl("/Service.php?Service=removeFacebookId")
let URL_ADD_INSTAGRAM_ID = getFullUrl("/Service.php?Service=addInstagramId")
let URL_REMOVE_INSTAGRAM_ID = getFullUrl("/Service.php?Service=removeInstagramId")
let URL_LOGOUT = getFullUrl("/Service.php?Service=logout")
let URL_ADD_USER_EXTRA_INFO = getFullUrl("/Service.php?Service=addExtraUserInfo")
let URL_USER_HAIR_INFO = getFullUrl("/Service.php?Service=getUserHairInfo")
let URL_HAIR_DRESSER_LIST = getFullUrl("/Service.php?Service=getHairDresserList")
let URL_ADD_CARD = getFullUrl("/Service.php?Service=addCardToCustomer")
let URL_ADD_TO_CART = getFullUrl("/Service.php?Service=addToCart")
let URL_GET_CART_ITEM = getFullUrl("/Service.php?Service=getCartItem")
let URL_DELETE_SERVICE_CART_ITEM = getFullUrl("/Service.php?Service=deleteServiceFromCart")
let URL_GET_CARD_LIST = getFullUrl("/Service.php?Service=getCardList")
let URL_DELETE_CARD = getFullUrl("/Service.php?Service=deletePaymentCard")
let URL_CREATE_CHARGES = getFullUrl("/Service.php?Service=CreateCharges")
let URL_CHECK_HAIR_DRESSER_AVAILABILITY = getFullUrl("/Service.php?Service=checkHairdresserAvailability")
let URL_BOOKING_APPOINTMENT = getFullUrl("/Service.php?Service=addBookingAppointment")
let URL_GET_BOOKING_APPOINTMNET = getFullUrl("/Service.php?Service=getAppointmentList")
let URL_ACCEPT_REJECT = getFullUrl("/Service.php?Service=acceptRejectAppointment")
let URL_GET_USER_APPOINTMNET = getFullUrl("/Service.php?Service=getUserAppointmentList")
let URL_ADD_REVIEW = getFullUrl("/Service.php?Service=addReview")
let URL_CONNECT_STRIPE = getFullUrl("/Service.php?Service=connectWithStripe")
let URL_GET_TRANSACTION_LIST = getFullUrl("/Service.php?Service=getTransactionList")
let URL_CANCEL_SERVICE = getFullUrl("/Service.php?Service=cancelService")
let URL_COMPLETE_SERVICE = getFullUrl("/Service.php?Service=completeService")
let URL_REVIEW_LIST = getFullUrl("/Service.php?Service=getReviewList")
let URL_UPDATE_DEVICE_TOKEN = getFullUrl("/Service.php?Service=updateDeviceToken")
let URL_RESET_BADGE = getFullUrl("/Service.php?Service=resetBadges")
let URL_BOOKING_DETAIL = getFullUrl("/Service.php?Service=getBookingDetail")
let URL_NOTIFICATION_LIST = getFullUrl("/Service.php?Service=getNotificationList")

//MARK:- FULL URL
func getFullUrl(_ urlEndPoint : String) -> String {
    return URL_BASE + urlEndPoint
}

