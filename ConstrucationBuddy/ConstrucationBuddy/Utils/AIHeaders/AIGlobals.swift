//
//  AIGlobals.swift
//  Swift3CodeStructure
//
//  Created by Ravi Alagiya on 25/11/2016.
//  Copyright © 2016 Ravi Alagiya. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import CoreLocation
import SwiftyJSON

//MARK: - GENERAL
let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

//MARK: - MANAGERS
let ServiceManager = AIServiceManager.sharedManager
//let UserManager = AIUser.sharedManager

//MARK:- Storyboard Outlet

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)


var SHARE_BASE_URL = "https://www.construcationbuddy.com/"
var SHARE_BASE_URL_IOS = "com.app.bidretriver"
var DOMAIN_URL = "https://construcationbuddy.page.link"

//MARK: - INSTAGRAM

var instaRedirectURL = "https://www.google.com/"
let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
let INSTAGRAM_CLIENT_ID  = "971443143324189"
let INSTAGRAM_SECRATE_KEY = "7acf78a35adc8263e1140713518012a7"

let FIREBASE_SERVER_KEY = "AAAA--ibYLY:APA91bGcZL1FJcefLhd9EaMYMptLdPHzt4lbuOM1DPwMf20sbugUYNUh-5uXC1ST-zL810AEWI_et01r4VziqmXGAX7khhdkyuDCZ88P91AT_yCKI9Zea1iPXJyNuqM6QM-jwjsEufpC"


//MARK: - APP SPECIFIC
let APP_NAME = "Consturction Buddy"

let APP_SECRET = ""
let USER_AGENT = ""

let userDefault = UserDefaults.standard

//MARK: - ERROR
let CUSTOM_ERROR_DOMAIN = "CUSTOM_ERROR_DOMAIN"
let CUSTOM_ERROR_USER_INFO_KEY = "CUSTOM_ERROR_USER_INFO_KEY"
let DEFAULT_ERROR = "Something went wrong, please try again later."
let INTERNET_ERROR = "Please check your internet connection and try again."

let VERSION_NUMBER = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
let BUILD_NUMBER = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
var SYSTEM_VERSION = UIDevice.current.systemVersion
var DEVICE_UUID = UIDevice.current.identifierForVendor?.uuidString

//MARK:- Key Store
let USER_LOGIN_INFO = "USER_LOGIN_INFO"
let USER_EXTRA_INFO = "USER_EXTRA_INFO"


var kCurrentUserLocation = CLLocation()
var isComeFromPush = false
var dictPushData = JSON()
var isOnAppointmentScreen = Bool()
var isOnUserUpcomingAppointmentScreen = Bool()

//MARK: - SCREEN SIZE
let NAVIGATION_BAR_HEIGHT:CGFloat = 64
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width

func GET_PROPORTIONAL_WIDTH (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/375)
}
func GET_PROPORTIONAL_HEIGHT (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_HEIGHT * height)/667)
}

func GET_PROPORTIONAL_WIDTH_CELL (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/375)
}
func GET_PROPORTIONAL_HEIGHT_CELL (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * height)/667)
}

// MARK: - KEYS FOR USERDEFAULTS

let DeviceToken = "DeviceToken"

let IS_SIGNUP = "isSignup"


//MARK: - COLORS
/*let APP_THEME_RED_COLOR                 = #colorLiteral(red: 0.9843137255, green: 0.3294117647, blue: 0.3725490196, alpha: 1)
let APP_THEME_YELLOW_COLOR              = #colorLiteral(red: 0.9764705882, green: 0.8156862745, blue: 0.1764705882, alpha: 1)
let APP_THEME_LIGHT_GRAY_COLOR          = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
let APP_THEME_DARK_GRAY_COLOR           = #colorLiteral(red: 0.5450980392, green: 0.5607843137, blue: 0.5843137255, alpha: 1)
let APP_THEME_GREEN_COLOR               = #colorLiteral(red: 0.2039215686, green: 0.2509803922, blue: 0.2352941176, alpha: 1)
let APP_THEME_SKY_COLOR                 = #colorLiteral(red: 0.3333333333, green: 0.8588235294, blue: 0.7960784314, alpha: 1)
let APP_THEME_BLUE_COLOR                = #colorLiteral(red: 0.1803921569, green: 0.3725490196, blue: 0.6549019608, alpha: 1)*/
let APP_THEME_BLUE_COLOR = #colorLiteral(red: 0.2156862745, green: 0.5333333333, blue: 0.5019607843, alpha: 1) // UIColor(named: "Theme")

//MARK:- print fonts

func printFonts()
{
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        print("Font Names = [\(names)]")
    }
}

//MARK: - Set Toaster

func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
}

func apiCallingFromLogout() {
    ServiceManager.callAPI(url: URL_LOGOUT, parameter: [:],isShowLoader: false,success: { (response) in
        print("Response \(response)")
        /*appDelegate.objUser = nil
        removeObjectForKey(USER_LOGIN_INFO)
        removeObjectForKey(USER_EXTRA_INFO)
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let nav = UINavigationController(rootViewController: obj)
        nav.isNavigationBarHidden = true
        appDelegate.window?.rootViewController = nav*/
    }, failure: { (error) in
        makeToast(strMessage: error)
    }, connectionFailed: {(msg) in
        makeToast(strMessage: msg)
    })
}

func forceLogout() {
   /* appDelegate.objUser = nil
    removeObjectForKey(USER_LOGIN_INFO)
    removeObjectForKey(USER_EXTRA_INFO)
    let obj = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    let nav = UINavigationController(rootViewController: obj)
    nav.isNavigationBarHidden = true
    appDelegate.window?.rootViewController = nav*/
}








