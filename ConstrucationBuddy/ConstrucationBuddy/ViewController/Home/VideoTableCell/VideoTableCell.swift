//
//  VideoTableCell.swift
//  ConstrucationBuddy
//
//  Created by Jaydeep on 29/12/20.
//

import UIKit

class VideoTableCell: UITableViewCell {
    
    @IBOutlet weak var btnPlayOutlet:UIButton!
    @IBOutlet weak var imgPost:UIImageView!
    @IBOutlet weak var btnMessageOutlet:UIButton!
    @IBOutlet weak var btnShareOutlet:UIButton!
    @IBOutlet weak var imgProfile:CustomImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMsg:UILabel!
    @IBOutlet weak var btnImageTap:UIButton!
    @IBOutlet weak var playerView:PlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgPost.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
