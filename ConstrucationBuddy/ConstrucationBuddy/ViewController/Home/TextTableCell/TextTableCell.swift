//
//  TextTableCell.swift
//  ConstrucationBuddy
//
//  Created by Jaydeep on 28/12/20.
//

import UIKit

class TextTableCell: UITableViewCell {
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgProfile:CustomImageView!
    @IBOutlet weak var btnMessageOutlet:UIButton!
    @IBOutlet weak var lblPostText:UILabel!
    @IBOutlet weak var btnShare:UIButton!
    @IBOutlet weak var btnImageTap:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
