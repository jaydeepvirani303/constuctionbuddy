//
//  HomeVC.swift
//  ConstrucationBuddy
//
//  Created by Jaydeep on 28/12/20.
//

import UIKit
import Photos
import MobileCoreServices
import AVKit
import BSImagePicker
import YangMingShan
import FirebaseDatabase
import FirebaseStorage
import SwiftyJSON
import SDWebImage
import FirebaseFirestore
import FirebaseDynamicLinks


class HomeVC: UIViewController, YMSPhotoPickerViewControllerDelegate {
    
    //MARK:- Variable Declration
    var picker = UIImagePickerController()
    var isSelectImage : Bool = true
    var isVideoUrl : URL?
    var arrImages = [UIImage]()
    var ref: DatabaseReference!
    
    typealias FileCompletionBlock = () -> Void
    var block: FileCompletionBlock?
    var arrPostList = [JSON]()
    
      private var conversations = [ObjectConversation]()
      private let manager = ConversationManager()
      private let userManager = UserManager()
      private let Msgmanager = MessageManager()
      private var currentUser: ObjectUser?
      var users = ObjectUser()
      var conversation = ObjectConversation()
        
    //MARK:- Outelt Zone
    @IBOutlet weak var txtPost:UITextView!
    @IBOutlet weak var tblHome:UITableView!
    
    @IBOutlet weak var viewMedia:UIView!
    @IBOutlet weak var imgMedia:UIImageView!
    @IBOutlet weak var btnPlay:UIButton!
    @IBOutlet weak var btnChatMessage:UIButton!
    @IBOutlet weak var viewPost:UIView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblHome.register(UINib(nibName: "TextTableCell", bundle: nil), forCellReuseIdentifier: "TextTableCell")
        tblHome.register(UINib(nibName: "VideoTableCell", bundle: nil), forCellReuseIdentifier: "VideoTableCell")
        tblHome.register(UINib(nibName: "ImageTextCell", bundle: nil), forCellReuseIdentifier: "ImageTextCell")
        ref = Database.database().reference()
        viewMedia.isHidden = true
        btnPlay.isHidden = true
        getRetriveData()
        self.fetchConversations()
        self.fetchProfile()
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBarWithBackNameButton(APP_NAME,isBackHidden: true)
        
        let logout = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(logOut))
        logout.tintColor = .white
        self.navigationItem.rightBarButtonItem = logout
        
        if(appDelegate.loginUserOBJ.user_type == "client"){
            self.viewPost.isHidden = false
            self.btnChatMessage.isHidden = false
        }else{
            self.viewPost.isHidden = true
            self.btnChatMessage.isHidden = true
        }
        
    }
    
}

//MAR:- Action Jone

extension HomeVC {
    
    
    func playVideo(url: URL) {
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        //        vc.videoGravity = .resizeAspectFill
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
    
    
    func validationMethod() -> String {
        if(txtPost.text?.trimString().count == 0){
            return "Please enter post description."
        }
        return ""
    }
    
    func startUploading(completion: @escaping FileCompletionBlock) {
        if arrImages.count == 0 {
            completion()
            return;
        }
        
        block = completion
        uploadImage(forIndex: 0)
    }
    
    func uploadImage(forIndex index:Int) {
        
        if index < arrImages.count {
            /// Perform uploading
            let data = arrImages[index].pngData()!
            
            let fileName = String(format: "%@.png", "\(NSDate().timeIntervalSince1970)_\(index)")
            
            FirFile.shared.upload(data: data, withName: fileName, block: { (url) in
                /// After successfully uploading call this method again by increment the **index = index + 1**
                print(url ?? "Couldn't not upload. You can either check the error or just skip this.")
                self.uploadImage(forIndex: index + 1)
            })
            return;
        }
        
        if block != nil {
            block!()
        }
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
    
    @IBAction func btnSendPost(_ sender:UIButton) {
        self.view.endEditing(true)
        if(validationMethod() != ""){
            makeToast(strMessage: validationMethod())
        }else{
            // API Call For Send Post
            let postId = UUID().uuidString
            SHOW_CUSTOM_LOADER()
            appDelegate.arrImgUrl.removeAll()
            startUploading {
                if(self.isVideoUrl != nil){
                    self.uploadVideoToFile { (strLink) in
                        
                        var param = [String:AnyObject]()
                        param["name"] = appDelegate.loginUserOBJ.username as AnyObject?
                        param["profile"] =  appDelegate.loginUserOBJ.profilePicLink == "" ? "" as AnyObject : appDelegate.loginUserOBJ.profilePicLink as AnyObject
                        param["text"] = self.txtPost.text as AnyObject?
                        param["uid"] = appDelegate.loginUserOBJ.id as AnyObject
                        param["video"] = strLink as AnyObject
                        param["image"] = appDelegate.arrImgUrl as AnyObject
                        param["id"] = postId as AnyObject
                        print(param)
                        self.ref.child("posts/\(postId)").setValue(param)
                        HIDE_CUSTOM_LOADER()
                        self.setBlankValue()
                        self.sendPushTOAllUser()
                        self.getRetriveData()
                    }
                }else{
                    var param = [String:AnyObject]()
                    param["name"] = appDelegate.loginUserOBJ.username as AnyObject?
                    param["profile"] =  appDelegate.loginUserOBJ.profilePicLink == "" ? "" as AnyObject : appDelegate.loginUserOBJ.profilePicLink as AnyObject
                    param["text"] = self.txtPost.text as AnyObject?
                    param["uid"] = appDelegate.loginUserOBJ.id as AnyObject
                    param["video"] = "" as AnyObject
                    param["image"] = appDelegate.arrImgUrl as AnyObject
                    param["id"] = postId as AnyObject
                    print(param)
                    self.ref.child("posts/\(postId)").setValue(param)
                    HIDE_CUSTOM_LOADER()
                    self.setBlankValue()
                    self.sendPushTOAllUser()
                }
                
                self.getRetriveData()
            }
            
            
        }
    }
    
    func sendPushTOAllUser() {
        
        Firestore.firestore().collection("Users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    
                    if let user_ID = document.data()["uid"] as? String {
                        if(user_ID != appDelegate.loginUserOBJ.id){
                            if let token = document.data()["fcm_token"] as? String {
                                print(token)
                                
                                self.sendPushNotification(to: token, title: appDelegate.loginUserOBJ.username ?? "", body: "New post added", badge: 1, id: token)
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    
    
    
    
    
    
    func setBlankValue()  {
        appDelegate.arrImgUrl.removeAll()
        self.arrImages.removeAll()
        self.txtPost.text = ""
        viewMedia.isHidden = true
        btnPlay.isHidden = true
        isVideoUrl = nil
    }
    
    
    func getRetriveData()  {
        SHOW_CUSTOM_LOADER()
        self.arrPostList = []
        let ref = Database.database().reference(withPath: "posts")
        ref.observeSingleEvent(of: .value, with: { snapshot in

            if !snapshot.exists() { return }
            print(snapshot) // Its print all values including Snap (User)
            print(snapshot.value!)
            
            guard let listingsDictionary = snapshot.value as? [String: Any] else { return }

            listingsDictionary.forEach({ (key, value) in
                print("value \(value)")
                print("key \(key)")
                self.arrPostList.append(JSON(value))
            })
            HIDE_CUSTOM_LOADER()
            self.tblHome.reloadData()
        })
        
    }
    
    

    func uploadVideoToFile( completion:((_ strLink:String) -> Void)!) -> Void {
        
        let data = NSData(contentsOf: self.isVideoUrl!)
        let name = "\(Int(Date().timeIntervalSince1970)).mp4"
        let path = NSTemporaryDirectory() + name
        
        do {
            try data?.write(to: URL(fileURLWithPath: path), options: .atomic)
        } catch {
            print(error)
        }
        
        let storageRef = Storage.storage().reference().child("Posts").child(appDelegate.loginUserOBJ.id).child(name)
        if let uploadData = data as Data? {
            storageRef.putData(uploadData, metadata: nil
                , completion: { (metadata, error) in
                    if let error = error {
                        makeToast(strMessage: error.localizedDescription)
                    }else{
                        storageRef.downloadURL { (url, error) in
                            guard let downloadURL = url else {
                                // Uh-oh, an error occurred!
                                return
                            }
                            print(downloadURL.absoluteURL.absoluteString)
                            completion(downloadURL.absoluteURL.absoluteString)
                        }
                        
                    }
                    
                    
            })
        }
    }
    
    
    
    @IBAction func btnPlayAction(_ sender:UIButton) {
        if(isVideoUrl != nil){
            playVideo(url: isVideoUrl!)
        }
    }
    
    @IBAction func btnSelectCameraAction(_ sender:UIButton) {
        PHPhotoLibrary.requestAuthorization({status in
            switch status {
            case .authorized:
                DispatchQueue.main.async {
                    //self.alertActionForGallary()
                    self.showDisplayAction()
                }
            case .denied:
                print("denied")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            // probably alert the user that they need to grant photo access
            case .notDetermined:
                print("not determined")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            case .restricted:
                print("restricted")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            @unknown default:
                print("")
            }
        })
    }
    
    @IBAction func btnSelectVideoAction(_ sender:UIButton) {
        PHPhotoLibrary.requestAuthorization({status in
            switch status {
            case .authorized:
                DispatchQueue.main.async {
                    self.alertActionForVideo()
                }
            case .denied:
                print("denied")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            // probably alert the user that they need to grant photo access
            case .notDetermined:
                print("not determined")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            case .restricted:
                print("restricted")
                DispatchQueue.main.async {
                    self.redirectToPhotos()
                }
            // probably alert the user that photo access is restricted
            @unknown default:
                print("")
            }
        })
    }
    
    func redirectToPhotos() {
        displayAlertWithTitle(APP_NAME, andMessage: "Please turn on photos permission from setting", buttons: ["Setting","Cancel"], completion: {(tag) in
            if tag == 0{
                let url = URL(string:UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(url!){
                    // can open succeeded.. opening the url
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
            }
            
        })
    }
    
    @objc func logOut() {
        
        displayAlertWithTitle(APP_NAME, andMessage: "Are you sure you want to logout?", buttons: ["Yes","No"]) { (index) in
            if(index == 0){
                UserManager().logout()
                let obj = mainStoryboard.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
                let nav = UINavigationController(rootViewController: obj)
                nav.isNavigationBarHidden = true
                appDelegate.window?.rootViewController = nav
            }
        }
    }
    
    
}

//MARK:- Tableview Delegate

extension HomeVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = arrPostList[indexPath.row]
        
        let arrPostImages = dict["image"].arrayObject
        if arrPostImages?.count != 0 && arrPostImages != nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTextCell") as! ImageTextCell
            if let arrCount = arrPostImages {
                cell.pageControl.numberOfPages = arrCount.count
                cell.arrImage = arrCount
            }
            cell.lblMsg.text = dict["text"].stringValue
            cell.lblName.text = dict["name"].stringValue
            cell.imgProfile.sd_setImage(with: URL(string: dict["profile"].stringValue), placeholderImage: UIImage(named: "ic_profile"), options: .lowPriority, context: nil)
            
            cell.collectionPhotos.reloadData()
            cell.btnMessageOutlet.tag = indexPath.row
            cell.btnMessageOutlet.addTarget(self, action: #selector(btnMsgAction), for: .touchUpInside)
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
            
            cell.btnImageTap.tag = indexPath.row
            cell.btnImageTap.addTarget(self, action: #selector(btnImageTapAction), for: .touchUpInside)
            
            
            if(appDelegate.loginUserOBJ.user_type == "client"){
               if(appDelegate.loginUserOBJ.id == dict["uid"].stringValue) {
                    cell.btnMessageOutlet.isHidden = true
                }else{
                    cell.btnMessageOutlet.isHidden = false
                }
            }else{
                 cell.btnMessageOutlet.isHidden = true
            }
            
            
            
            return cell
        }
        if dict["video"].stringValue != "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableCell") as! VideoTableCell
            cell.btnPlayOutlet.isHidden = false
            
            cell.btnPlayOutlet.tag = indexPath.row
            cell.btnPlayOutlet.addTarget(self, action: #selector(btnPlayTableAction), for: .touchUpInside)
            
            cell.btnMessageOutlet.tag = indexPath.row
            cell.btnMessageOutlet.addTarget(self, action: #selector(btnMsgAction), for: .touchUpInside)
            cell.lblMsg.text = dict["text"].stringValue
            cell.lblName.text = dict["name"].stringValue
            cell.imgProfile.sd_setImage(with: URL(string: dict["profile"].stringValue), placeholderImage: UIImage(named: "ic_profile"), options: .lowPriority, context: nil)
            
            cell.btnImageTap.tag = indexPath.row
            cell.btnImageTap.addTarget(self, action: #selector(btnImageTapAction), for: .touchUpInside)
            
            if let url = URL(string: dict["video"].stringValue) {
                let avPlayer = AVPlayer(url: url)
                cell.playerView.playerLayer.videoGravity = .resizeAspectFill
                cell.playerView?.playerLayer.player = avPlayer
            }
            
            if(appDelegate.loginUserOBJ.user_type == "client"){
               if(appDelegate.loginUserOBJ.id == dict["uid"].stringValue) {
                    cell.btnMessageOutlet.isHidden = true
                }else{
                    cell.btnMessageOutlet.isHidden = false
                }
            }else{
                 cell.btnMessageOutlet.isHidden = true
            }
            
            cell.btnShareOutlet.tag = indexPath.row
            cell.btnShareOutlet.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextTableCell") as! TextTableCell
        cell.lblName.text = dict["name"].stringValue
        cell.lblPostText.text = dict["text"].stringValue
        cell.imgProfile.sd_setImage(with: URL(string: dict["profile"].stringValue), placeholderImage: UIImage(named: "ic_profile"), options: .lowPriority, context: nil)
        cell.btnMessageOutlet.tag = indexPath.row
        cell.btnMessageOutlet.addTarget(self, action: #selector(btnMsgAction), for: .touchUpInside)
        
        cell.btnImageTap.tag = indexPath.row
        cell.btnImageTap.addTarget(self, action: #selector(btnImageTapAction), for: .touchUpInside)
        
        if(appDelegate.loginUserOBJ.user_type == "client"){
           if(appDelegate.loginUserOBJ.id == dict["uid"].stringValue) {
                cell.btnMessageOutlet.isHidden = true
            }else{
                cell.btnMessageOutlet.isHidden = false
            }
        }else{
             cell.btnMessageOutlet.isHidden = true
        }
        
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = (cell as? VideoTableCell) else { return };
        let visibleCells = tableView.visibleCells
        let minIndex = visibleCells.startIndex
        if tableView.visibleCells.firstIndex(of: cell) == minIndex {
            videoCell.playerView.player?.play()
        }
    }
        
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = cell as? VideoTableCell else { return }
        videoCell.playerView.player?.pause()
        videoCell.playerView.player = nil;
    }
    
    @IBAction func btnMsgAction(_ sender:UIButton)  {
        let dict = arrPostList[sender.tag]
        
        
        self.users.id = dict["uid"].stringValue
        self.users.username = dict["name"].stringValue
        self.users.profilePicLink = dict["profile"].stringValue
        guard let currentID = self.userManager.currentUserID() else { return }
        let VC = chatStoryboard.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        VC.isTabBar = false
        VC.isAddProduct = false
        VC.isTab = false
        if let conversation = self.conversations.filter({$0.userIDs.contains(self.users.id)}).first {
          VC.conversation = conversation
          self.navigationController?.pushViewController(VC, animated: true)
          return
        }
        let conversation = ObjectConversation()
        conversation.userIDs.append(contentsOf: [currentID, self.users.id])
        conversation.isRead = [currentID: true, self.users.id: true]
        VC.conversation = conversation
        VC.user = self.users
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
     @IBAction func btnPlayTableAction(_ sender:UIButton)  {
        let dict = arrPostList[sender.tag]
        if let url = URL(string: dict["video"].stringValue) {
            playVideo(url: url)
        }
    }
    
    @IBAction func btnImageTapAction(_ sender:UIButton)  {
        
        let dict = arrPostList[sender.tag]
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        obj.userID = dict["uid"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnShareAction(_ sender:UIButton)  {
        
        let dict = arrPostList[sender.tag]
        
            
        guard let link = URL(string: "\(SHARE_BASE_URL)/\(dict["id"].stringValue)") else { return }
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: DOMAIN_URL)
        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.app.ConstrucationBuddy")
        linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.ConstrucationBuddy")

//        guard let longDynamicLink = linkBuilder?.url else { return }
//        print("The long URL is: \(longDynamicLink)")
//        linkBuilder.
//        linkBuilder?.options = DynamicLinkComponentsOptions()
        linkBuilder?.options?.pathLength = .short
        linkBuilder?.shorten() { url, warnings, error in
//          guard let url = url, error != nil else { return }
            print("The short URL is: \(url?.absoluteString)")
            
            let text = url?.absoluteString

            // set up activity view controller
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            self.present(activityViewController, animated: true, completion: nil)
            
        }
//        let text = dict["text"].stringValue
        
        
    }
    
    
    @IBAction func btnMessageAction(_ sender:UIButton)  {
        let obj = chatStoryboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - UIImagePickerController

extension HomeVC:UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    func alertActionForVideo()  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: APP_NAME, message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.picker = UIImagePickerController()
            self.picker.mediaTypes = ["public.movie"]
            self.picker.allowsEditing = true
            self.picker.delegate = self
            self.picker.sourceType = .photoLibrary
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker, animated: true, completion: nil)
            
        }
        
        
        let cameraAction = UIAlertAction(title: "Video", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker = UIImagePickerController()
                self.picker.allowsEditing = true
                self.picker.delegate = self
                self.picker.videoMaximumDuration = 15
                self.picker.sourceType = .camera
                self.picker.mediaTypes = [kUTTypeMovie as String]
                self.picker.modalPresentationStyle = .fullScreen
                self.picker.videoQuality = .typeHigh
                self.present(self.picker, animated: true, completion: {
                    self.picker.cameraFlashMode = .off
                })
            } else {
                makeToast(strMessage: "Sorry, this device has not camera.")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(gallaryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertActionForGallary()  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: APP_NAME, message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            //            self.picker.delegate = self
            //            self.picker.sourceType = .photoLibrary
            //            self.picker.allowsEditing = true
            //            //            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            //            self.picker.allowsEditing = false
            //            self.picker.mediaTypes = ["public.image"]
            //            self.picker.modalPresentationStyle = .fullScreen
            //            self.present(self.picker, animated: true, completion: nil)
            
            self.alertActionForMultipleGallry()
        }
        
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.allowsEditing = true
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            } else {
                makeToast(strMessage: "Sorry, this device has not camera.")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(gallaryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func alertActionForMultipleGallry(){
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 3
        imagePicker.settings.theme.selectionStyle = .numbered
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.settings.selection.unselectOnReachingMax = true
        
        let start = Date()
        self.presentImagePicker(imagePicker, select: { (asset) in
            print("Selected: \(asset)")
        }, deselect: { (asset) in
            print("Deselected: \(asset)")
        }, cancel: { (assets) in
            print("Canceled with selections: \(assets)")
        }, finish: { (assets) in
            print("Finished with selections: \(assets)")
            SHOW_CUSTOM_LOADER()
            DispatchQueue.main.async {
                self.arrImages.removeAll()
                for i in 0..<assets.count{
                    self.getImageFromAsset(asset: assets[i], callback: { (image) in
                        self.arrImages.append(image)
                    })
                }
                HIDE_CUSTOM_LOADER()
            }
        }, completion: {
            let finish = Date()
            print(finish.timeIntervalSince(start))
        })
    }
    
    
    
    func showDisplayAction() {
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 3
        //pickerViewController.delegate = self
        pickerViewController.theme.tintColor = #colorLiteral(red: 0.2156862745, green: 0.5333333333, blue: 0.5019607843, alpha: 1)  //UIColor(named: "Theme")
        pickerViewController.theme.orderTintColor = #colorLiteral(red: 0.2156862745, green: 0.5333333333, blue: 0.5019607843, alpha: 1) //UIColor(named: "Theme")
        pickerViewController.theme.cameraVeilColor = #colorLiteral(red: 0.2156862745, green: 0.5333333333, blue: 0.5019607843, alpha: 1) //UIColor(named: "Theme")
        
        //        pickerViewController.modalPresentationStyle = .fullScreen
        //        self.present(pickerViewController, animated: true, completion: nil)
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
        
        
    }
    
    func getImageFromAsset(asset:PHAsset,callback:@escaping (_ result:UIImage) -> Void) -> Void {
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.fast
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        requestOptions.isNetworkAccessAllowed = true
        requestOptions.isSynchronous = true
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (currentImage, info) in
            callback(currentImage!)
        })
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            isSelectImage = true
            imgMedia.image = chosenImage
            viewMedia.isHidden = false
            self.btnPlay.isHidden = true
        } else if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            isSelectImage = true
            imgMedia.image = chosenImage
            viewMedia.isHidden = false
            self.btnPlay.isHidden = true
        } else if let videoURL = info[.mediaURL] as? URL {
            isSelectImage = false
            
            let data = NSData(contentsOf: videoURL as URL)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
            compressVideo(inputURL: videoURL , outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {
                        return
                    }
                    print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                    DispatchQueue.main.async {
                        self.getThumbnailImageFromVideoUrl(url: compressedURL) { (img) in
                            self.btnPlay.isHidden = false
                            self.viewMedia.isHidden = false
                            self.isVideoUrl = compressedURL
                            self.imgMedia.image = img
                        }
                    }
                case .failed:
                    break
                case .cancelled:
                    break
                @unknown default:
                    break
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetHighestQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    
    
    
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            self.arrImages.removeAll()
            
            for asset: PHAsset in photoAssets
            {
                let scale = UIScreen.main.scale
                let targetSize = CGSize(width: (self.imgMedia.width - 20*2) * scale, height: (self.imgMedia.height - 20*2) * scale)
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: options, resultHandler: { (image, info) in
                    
                    if(image != nil ){
                        self.arrImages.append(image!)
                    }
                    
                })
            }
            // Assign to Array with images
            if(self.arrImages.count != 0){
                self.imgMedia.image = self.arrImages[0]
                self.isSelectImage = true
                self.viewMedia.isHidden = false
                self.btnPlay.isHidden = true
            }
        }
    }
}



class FirFile: NSObject {
    
    /// Singleton instance
    static let shared: FirFile = FirFile()
    
    /// Path
    let kFirFileStorageRef = Storage.storage().reference().child("Posts").child(appDelegate.loginUserOBJ.id)
    
    /// Current uploading task
    var currentUploadTask: StorageUploadTask?
    
    func upload(data: Data,
                withName fileName: String,
                block: @escaping (_ url: String?) -> Void) {
        
        // Create a reference to the file you want to upload
        let fileRef = kFirFileStorageRef.child(fileName)
        
        /// Start uploading
        upload(data: data, withName: fileName, atPath: fileRef) { (url) in
            block(url)
        }
    }
    
    
    func upload(data: Data,
                withName fileName: String,
                atPath path:StorageReference,
                block: @escaping (_ url: String?) -> Void) {
        
        // Upload the file to the path
        self.currentUploadTask = path.putData(data, metadata: nil) { (metaData, error) in
            guard let metadata = metaData else {
                // Uh-oh, an error occurred!
                block(nil)
                return
            }
            // Metadata contains file metadata such as size, content-type.
            // let size = metadata.size
            // You can also access to download URL after upload.
            path.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    block(nil)
                    return
                }
                appDelegate.arrImgUrl.append(downloadURL.absoluteURL.absoluteString)
                print(downloadURL.absoluteURL.absoluteString)
                block(downloadURL.absoluteURL.absoluteString)
            }
        }
    }
    
    func cancel() {
        self.currentUploadTask?.cancel()
    }
}


extension HomeVC {
  
  func fetchConversations() {
    manager.currentConversations {[weak self] conversations in
      self?.conversations = conversations.sorted(by: {$0.timestamp > $1.timestamp})
      //self?.playSoundIfNeeded()
    }
  }
  
  func fetchProfile() {
    userManager.currentUserData {[weak self] user in
      self?.currentUser = user
    }
  }
  
    func send(_ message: ObjectMessage) {
        Msgmanager.create(message, conversation: conversation, count: 1) {[weak self] response in
        guard let weakSelf = self else { return }
        if response == .failure {
          weakSelf.showAlert()
          return
        }
        weakSelf.conversation.timestamp = Int(Date().timeIntervalSince1970)
        switch message.contentType {
        case .none: weakSelf.conversation.lastMessage = message.message
        case .photo: weakSelf.conversation.lastMessage = "Attachment"
        case .location: weakSelf.conversation.lastMessage = "Location"
        default: break
        }
        if let currentUserID = UserManager().currentUserID() {
          weakSelf.conversation.isRead[currentUserID] = true
        }
        ConversationManager().create(weakSelf.conversation)
      }
    }
    
  func playSoundIfNeeded() {
    guard let id = userManager.currentUserID() else { return }
    if conversations.last?.isRead[id] == false {
      AudioService().playSound()
    }
  }
    
    
    
    func sendPushNotification(to token: String, title: String, body: String,badge : Int ,id : String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body,"sound":"default","badge":badge],
                                           "data" : ["user" : id]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(FIREBASE_SERVER_KEY)", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
    
    
    
}





extension AVAsset {

    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
