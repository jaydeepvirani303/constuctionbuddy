//
//  ImageTextCell.swift
//  ConstrucationBuddy
//
//  Created by Zero thirteen on 03/01/21.
//

import UIKit
import SDWebImage

class ImageTextCell: UITableViewCell {
    
    var arrImage = [Any]()
    
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgProfile:CustomImageView!
    @IBOutlet weak var btnMessageOutlet:UIButton!
//    @IBOutlet weak var lblPostText:UILabel!
    @IBOutlet weak var btnShare:UIButton!
    @IBOutlet weak var collectionPhotos:UICollectionView!
    @IBOutlet weak var pageControl:UIPageControl!
    @IBOutlet weak var lblMsg:UILabel!
    @IBOutlet weak var btnImageTap:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionPhotos.delegate = self
        collectionPhotos.dataSource = self
        collectionPhotos.register(UINib(nibName: "ProfileCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProfileCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


//MARK :- Delegate

extension ImageTextCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
        
        cell.imgSlider.sd_setImage(with: URL(string: arrImage[indexPath.row] as! String), placeholderImage: UIImage(named: "ic_placeholder"), options: .lowPriority, context: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width, height: collectionView.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
}

