//
//  ClientRegisterViewController.swift
//  ConstrucationBuddy
//
//  Created by Zero thirteen on 27/12/20.
//

import UIKit
import FirebaseStorage
import FirebaseFirestore

class ClientRegisterViewController: UIViewController {
    
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtAcType: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgProfileSet: UIImageView!
    @IBOutlet weak var imgTraderSet: UIImageView!
    
    var isSelectFromProfile:Bool = true
    var pickerRepeat: UIPickerView!
    
    private let manager = UserManager()
    private let userManager = UserManager()
    var user = ObjectUser()
    
    var arrSelectValue = ["Unlimited contracting","G+12","G+4","G+1","Technical Services LLC","MEP works","Aluminum works","Carpentry","Steel Fabrication and Shades","Steel + Shutting works","Cleaning","Labor supply","Transport","Decoration works","Gypsum works","Paint works","Hardware supply","Block,Plaster,Tile,Marble fixing","Consultant","Developer","Others"]
    
    var imgProfile : UIImage?
    var imgTrader : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        doSetupUI()
    }
    
    func doSetupUI()  {
        
        pickerRepeat = UIPickerView()
        pickerRepeat.backgroundColor = UIColor.white
        pickerRepeat.dataSource = self
        pickerRepeat.delegate = self
        txtAcType.inputView = pickerRepeat
        
        showToolBarinRepeat()
    }
    
    
    func showToolBarinRepeat() {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.tintColor = #colorLiteral(red: 0.2156862745, green: 0.5333333333, blue: 0.5019607843, alpha: 1) //UIColor(named: "Theme")
        let cancelButton = UIBarButtonItem(title: "Remove", style: .plain, target: self, action: #selector(cancelRepeat));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneRepeat));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        txtAcType.inputAccessoryView = toolbar
    }
    
    @objc func doneRepeat(){
        self.view.endEditing(true)
        if(txtAcType.text?.trimString() == ""){
            txtAcType.text = arrSelectValue[0]
        }
    }
    @objc func cancelRepeat(){
        self.view.endEditing(true)
        txtAcType.text = ""
    }
    
    
    func validationMethod() -> String {
        
        if(txtCompanyName.text?.trimString().count == 0){
            return "Please enter a company name."
        }
        
        if(txtAcType.text?.trimString().count == 0){
            return "Please enter account type."
        }
        
        if(txtUsername.text?.trimString().count == 0){
            return "Please enter username."
        }
        
        
        if(txtEmail.text?.trimString().count == 0){
            return "Please enter email."
        }
        
        if((txtEmail.text?.isValidEmail())! == false){
            return "Please enter valid email address."
        }
        
        if(txtPassword.text! == ""){
            return "Please enter password."
        }
        
        if(imgProfile == nil){
            return "Please select profile image."
        }
        
        if(imgTrader == nil){
            return "Please select trader image."
        }
        
        return ""
    }
    
    
    func alertActionForCamare()  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: APP_NAME, message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let picker = UIImagePickerController.init()
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            picker.modalPresentationStyle = .fullScreen
            picker.delegate = self
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
            
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = UIImagePickerController.init()
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.cameraCaptureMode = .photo
                picker.modalPresentationStyle = .fullScreen
                picker.delegate = self
                picker.allowsEditing = false
                self.present(picker,animated: true,completion: nil)
            } else {
                displayAlertWithMessage("Sorry, this device has not camera.")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(gallaryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func setFirebaseSignupAPI()
    {
        
        user.acctype = self.txtAcType.text
        user.companyname = self.txtCompanyName.text
        user.fcm_token = getString(key: "FCM_TOKEN")
        user.trdlicense = ""
        user.user_type = "client"
        user.username = txtUsername.text
        user.email = self.txtEmail.text
        user.password = self.txtPassword.text
        user.profilePicLink = ""
        user.profilePic = imgProfile
        
        SHOW_CUSTOM_LOADER()
        manager.register(user: user) {[weak self] response in
            HIDE_CUSTOM_LOADER()
            switch response {
            case .success:
                guard let currentID = self?.userManager.currentUserID() else { return }
                UserManager().userData(for: UserManager().currentUserID()!) { (objUser) in
                    self?.user = objUser ?? ObjectUser()
                    appDelegate.loginUserOBJ = objUser ?? ObjectUser()
                    self?.uploadProfileImage()
                }
                
            case .failure:
                makeToast(strMessage: "Error in registration")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    
    func uploadProfileImage()  {
        SHOW_CUSTOM_LOADER()
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let imageData = imgTrader?.scale(to: CGSize(width: 350, height: 350))?.jpegData(compressionQuality: 0.3)
        
        let riversRef = storageRef.child("Contractors|Clients/TradeLicence/\(txtCompanyName!.text!)/\(txtUsername!.text!)").child(self.user.id + ".jpg")
        
        let uploadMetadata = StorageMetadata()
        uploadMetadata.contentType = "image/jpg"
        riversRef.putData(imageData!, metadata: uploadMetadata) { (_, error) in
            HIDE_CUSTOM_LOADER()
            guard error.isNone else {  return }
            riversRef.downloadURL(completion: { (url, err) in
                if let downloadURL = url?.absoluteString {
                    self.user.trdlicense = downloadURL
                    
                    guard let currentUserID = UserManager().currentUserID() else { return }
                    let usersRef = Firestore.firestore().collection("Users").document(currentUserID)
                    usersRef.setData(["trdlicense": downloadURL], merge: true)
                    
                    
                    UserManager().userData(for: UserManager().currentUserID()!) { (objUser) in
                        appDelegate.loginUserOBJ = objUser ?? ObjectUser()
                        let obj = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    
                    return
                }
                
            })
        }
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        
        if(validationMethod() != ""){
            makeToast(strMessage: validationMethod())
            //displayAlertWithMessage(validationMethod())
        }else{
            
            // Insert API Call
            setFirebaseSignupAPI()
            
        }
    }
    
    
    @IBAction func btnProfileAction(_ sender: Any) {
        isSelectFromProfile = true
        alertActionForCamare()
    }
    
    
    @IBAction func btnTradeAction(_ sender: Any) {
        isSelectFromProfile = false
        alertActionForCamare()
    }
    
}


extension ClientRegisterViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrSelectValue.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrSelectValue[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        txtAcType.text = arrSelectValue[row]
    }
    
}


extension ClientRegisterViewController : UIImagePickerControllerDelegate,
UINavigationControllerDelegate, UIActionSheetDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if(isSelectFromProfile){
                self.imgProfile = image
                imgProfileSet.image = image
            }else{
                self.imgTrader = image
                imgTraderSet.image = image
            }
            picker.dismiss(animated: true, completion: nil)
        }
    }
}
