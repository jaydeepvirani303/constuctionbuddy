//
//  ProfileViewController.swift
//  ConstrucationBuddy
//
//  Created by Zero thirteen on 31/12/20.
//

import UIKit

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblCompanyName:UILabel!
    @IBOutlet weak var lblAccountName:UILabel!
    @IBOutlet weak var imgLincense:UIImageView!
    
    
    var userID:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SHOW_CUSTOM_LOADER()
        UserManager().userData(for: userID!) { (objUser) in
            HIDE_CUSTOM_LOADER()
          
            self.lblName.text = objUser?.username
            self.lblAccountName.text = objUser?.acctype
            self.lblCompanyName.text = objUser?.companyname
            
            self.imgProfile.sd_setImage(with: URL(string: (objUser?.profilePicLink)!), placeholderImage: UIImage(named: "ic_profile"), options: .lowPriority, context: nil)
            
            self.imgProfile.sd_setImage(with: URL(string: (objUser?.trdlicense)!), placeholderImage: UIImage(named: "ic_placeholder"), options: .lowPriority, context: nil)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBarWithBackNameButton(APP_NAME,isBackHidden: false)
    }

}
