//
//  RegisterMemberViewController.swift
//  ConstrucationBuddy
//
//  Created by Zero thirteen on 27/12/20.
//

import UIKit

class RegisterMemberViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    private let manager = UserManager()
    private let userManager = UserManager()
    var user = ObjectUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func validationMethod() -> String {
                
        if(txtUsername.text?.trimString().count == 0){
            return "Please enter username."
        }
        
        
        if(txtEmail.text?.trimString().count == 0){
            return "Please enter email."
        }
        
        if((txtEmail.text?.isValidEmail())! == false){
            return "Please enter valid email address."
        }
        
        if(txtPassword.text! == ""){
            return "Please enter password."
        }
        
        return ""
    }
    
    func setFirebaseSignupAPI()
    {
        
        user.fcm_token = getString(key: "FCM_TOKEN")
        user.user_type = "member"
        user.username = txtUsername.text
        user.email = self.txtEmail.text
        user.password = self.txtPassword.text
        
        SHOW_CUSTOM_LOADER()
        manager.register(user: user) {[weak self] response in
        HIDE_CUSTOM_LOADER()
          switch response {
          case .success:
            guard let currentID = self?.userManager.currentUserID() else { return }
            UserManager().userData(for: UserManager().currentUserID()!) { (objUser) in
                                  appDelegate.loginUserOBJ = objUser ?? ObjectUser()
                let obj = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self?.navigationController?.pushViewController(obj, animated: true)
            }
          case .failure:
                makeToast(strMessage: "Error in registration")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
   @IBAction func btnRegisterAction(_ sender: Any) {
          if(validationMethod() != ""){
              makeToast(strMessage: validationMethod())
              //displayAlertWithMessage(validationMethod())
          }else{
              
              // Insert API Call
               setFirebaseSignupAPI()
              
          }
    }
      
}
