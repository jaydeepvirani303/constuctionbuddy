//
//  LoginViewController.swift
//  ConstrucationBuddy
//
//  Created by Zero thirteen on 27/12/20.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func validationMethod() -> String {
                        
        if(txtEmail.text?.trimString().count == 0){
            return "Please enter email."
        }
        
        if((txtEmail.text?.isValidEmail())! == false){
            return "Please enter valid email address."
        }
        
        if(txtPassword.text! == ""){
            return "Please enter password."
        }
        
        return ""
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        
        if(validationMethod() != ""){
            makeToast(strMessage: validationMethod())
        }else{
            
            let user = ObjectUser()
            user.email = txtEmail.text
            user.password = txtPassword.text
            SHOW_CUSTOM_LOADER()
            UserManager().login(user: user) {[weak self] response in
                HIDE_CUSTOM_LOADER()
                switch response {
                case .failure:
                    makeToast(strMessage: "Please enter valid username & password.")
                case .success:
                    
                    UserManager().userData(for: UserManager().currentUserID()!) { (objUser) in
                       appDelegate.loginUserOBJ = objUser ?? ObjectUser()
                        let obj = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self?.navigationController?.pushViewController(obj, animated: true)
                    }
                    
                }
            }
            
        }
        
           
     }
    
}
