//
//  MessageTableViewCell.swift
//  Sherpa
//
//  Created by Ghanshyam on 07/01/20.
//  Copyright © 2020 Unique Company. All rights reserved.
//

import UIKit
import SDWebImage

protocol MessageTableViewCellDelegate: class {
  func messageTableViewCellUpdate()
}

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView?
    @IBOutlet weak var messageTextView: UITextView?
    @IBOutlet weak var viewSendMessage: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewReceiveMsg: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var backButtonDismiss :((Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePic?.applyCircle()

        lblName.textColor = UIColor.clear
        lblName.font = themeFont(size: 8, fontname: .regular)
        
        lblUserName.textColor = UIColor.black
        lblUserName.font = themeFont(size: 8, fontname: .regular)
        
        messageTextView?.font = themeFont(size: 14, fontname: .regular)
                
    }
    
    func set(_ message: ObjectMessage) {
      
      lblName.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp)))
        messageTextView?.text = message.message
      guard let imageView = profilePic else { return }
      guard let userID = message.ownerID else { return }
      ProfileManager.shared.userData(id: userID) { user in
        let urlString = user?.profilePicLink
        imageView.setImage(url: URL(string: urlString ?? ""), img: #imageLiteral(resourceName: "ic_profile"))
        self.lblName.text = user?.username
      }
       lblUserName.text = message.fullName
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
class MessageAttachmentTableViewCell: MessageTableViewCell {
  
  @IBOutlet weak var attachmentImageView: UIImageView!
  @IBOutlet weak var attachmentImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var attachmentImageViewWidthConstraint: NSLayoutConstraint!
  weak var delegate: MessageTableViewCellDelegate?
  
  override func prepareForReuse() {
    super.prepareForReuse()
    //attachmentImageView.cancelDownload()
    attachmentImageView.image = nil
    attachmentImageViewHeightConstraint.constant = 250 / 1.3
    attachmentImageViewWidthConstraint.constant = 250
  }
}
