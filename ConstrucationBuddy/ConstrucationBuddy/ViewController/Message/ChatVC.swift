//
//  ChatVC.swift
//  Barber
//
//  Created by iMac on 29/12/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    //MARK:- Variable Declration
    
    var isCheck : Bool?
    private var conversations = [ObjectConversation]()
    private let manager = ConversationManager()
    private let userManager = UserManager()
    private var currentUser: ObjectUser?
    var lblNoDataFound = UILabel()
    var arrSearchUserList = [ObjectConversation]()
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        tblView.tableFooterView = UIView()
        
        fetchMessageConversation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBarWithImage(name: "Mesages", image: "", isBackHidden: false)
    }

}

//MARK: - MESSAGE METHODS

extension ChatVC {
    
    
    func fetchMessageConversation()
    {
        SHOW_CUSTOM_LOADER()
        manager.currentConversations {[weak self] conversations in
          self?.conversations = conversations.sorted(by: {$0.timestamp > $1.timestamp})
            
            self?.arrSearchUserList = self?.conversations ?? []
            self?.tblView.reloadData()
            self?.playSoundIfNeeded()
            HIDE_CUSTOM_LOADER()
        }
    }
    
    func playSoundIfNeeded() {
      guard let id = userManager.currentUserID() else { return }
      if conversations.last?.isRead[id] == false {
        AudioService().playSound()
      }
    }
}

//MARK: - TABLEVIEW DELEGATE DATASOURCE METHODS
extension ChatVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.conversations.count == 0{
            lblNoDataFound.isHidden = false
            lblNoDataFound.text = "No any Recent chat here"
            lblNoDataFound.textAlignment = NSTextAlignment.center
            lblNoDataFound.textColor = UIColor.black
            lblNoDataFound.font = themeFont(size: 16, fontname: .regular)
            lblNoDataFound.center = self.tblView.center
            self.tblView.backgroundView = lblNoDataFound
            return 0
        }
        lblNoDataFound.isHidden = true
        return self.conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! ChatCell
        cell.set(conversations[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = chatStoryboard.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        VC.conversation = conversations[indexPath.row]
        manager.markAsRead(conversations[indexPath.row])
        VC.isCheck = true
        VC.isTab = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 75
    }
   
}
extension ChatVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,  replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            searchProduct(text: updatedText)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func searchProduct(text:String) {
        conversations = arrSearchUserList.filter({ (objModel) -> Bool in
            let userID = UserManager().currentUserID() ?? ""
            guard let id = objModel.userIDs.filter({$0 != userID}).first else { return false }
            var tmp: NSString = ""
            ProfileManager.shared.userData(id: id) { profile in
                tmp = profile?.username as NSString? ?? ""
            }
            let range = tmp.range(of: text, options: .caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(text.isEmpty || text == ""){
            conversations = arrSearchUserList
            self.lblNoDataFound.isHidden = true
            self.tblView.isHidden = false
            tblView.reloadData()
        }
        if(self.conversations.count > 0){
            self.lblNoDataFound.isHidden = true
           // self.tblView.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = false
            //self.tblView.isHidden = true
        }
        tblView.reloadData()
    }
}
