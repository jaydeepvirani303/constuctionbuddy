//
//  ChatCell.swift
//  Purple Heylo
//
//  Created by Ghanshyam on 29/09/20.
//  Copyright © 2020 Unique Company. All rights reserved.
//

import UIKit
import SDWebImage


class ChatCell: UITableViewCell {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTiime: UILabel!
    @IBOutlet weak var viewCount: UIView!
    @IBOutlet weak var lblCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgProfile.applyCircle()
        viewCount.applyCircle()
//        viewCount.
        imgProfile.applyBorder(.white, width: 1.0)
       // imgProfile.applyShadowDefault()
        
        lblName.textColor = UIColor.black
        lblName.font = themeFont(size: 16, fontname: .medium)
        
        lblMessage.textColor = UIColor.black
        lblMessage.font = themeFont(size: 12, fontname: .regular)
        
        lblTiime.textColor = UIColor.black
        lblTiime.font = themeFont(size: 8, fontname: .regular)
        
        lblCount.textColor = .white
        lblCount.font = themeFont(size: 12, fontname: .regular)
        
    }

    //MARK: Private properties
    let userID = UserManager().currentUserID() ?? ""
    
    //MARK: Public methods
    func set(_ conversation: ObjectConversation) {
        lblTiime.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(conversation.timestamp)))
      lblMessage.text = conversation.lastMessage
        
      guard let id = conversation.userIDs.filter({$0 != userID}).first else { return }
      let isRead = conversation.isRead[userID] ?? true
      if !isRead {
        lblMessage.font = themeFont(size: 12, fontname: .regular)
        lblMessage.textColor = UIColor.black
        viewCount.isHidden = false
        lblCount.text = "\(conversation.unreadCount)"
      }else
      {
        lblMessage.font = themeFont(size: 12, fontname: .regular)
        lblMessage.textColor = UIColor.black
        viewCount.isHidden = true
        }
      ProfileManager.shared.userData(id: id) {[weak self] profile in
        self?.lblName.text = profile?.username
        let urlString = profile?.profilePicLink
        self?.imgProfile.sd_imageIndicator =  SDWebImageActivityIndicator.gray
        self?.imgProfile.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: UIImage(named: "ic_profile"))
      }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
