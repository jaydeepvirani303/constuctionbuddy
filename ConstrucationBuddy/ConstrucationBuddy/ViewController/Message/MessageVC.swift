//
//  MessageVC.swift
//  Barber
//
//  Created by iMac on 29/12/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseFirestore

class MessageVC: UIViewController,KeyboardHandler {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightForWriteMsg: NSLayoutConstraint!
    @IBOutlet weak var typeMessageView: UIView!
    
    //MARK:- Variable Declaraion
    
    private let manager = MessageManager()
    private var messages = [ObjectMessage]()
    var users = ObjectUser()
    private let userManager = UserManager()
    private var currentUser: ObjectUser?
    var badge = 0
    var current = ConversationManager()
    var arrconversation = ObjectConversation()
    var isCheck = true
    var isTab : Bool?
    
    var conversation = ObjectConversation()
    var user = ObjectUser()

    var safeAreaInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
     var bottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.bottom + 50
        } else {
            // Fallback on earlier versions
            return bottomLayoutGuide.length + 50
        }
     }
    
    var isTabBar = true
    var isAddProduct = true
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchProfile()
        IQKeyboardManager.shared.enable = false
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBarWithImage(name: user.profilePicLink ?? "", image: user.username ?? "", isBackHidden: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        current.markAsRead(conversation)
    }
    
    func setUI()
    {
        barBottomConstraint.constant = 0
        
        typeMessageView.applyBorder(UIColor.gray.withAlphaComponent(0.4), width: 2.0)
        typeMessageView.applyCircle()
        addKeyboardObservers() {[weak self] state in
          guard state else { return }
            self?.tableView.scroll(to: .bottom, animated: true)
        }
        
        fetchUserName()
        fetchMessages()
    }
    
    @IBAction func btnSendMsg_Click(_ sender: Any) {
        guard let text = inputTextField.text, !text.isEmpty else { return }
        let message = ObjectMessage()
        message.message = text
        message.ownerID = UserManager().currentUserID()
        inputTextField.text = nil
        self.singleConversation(message)
    }
 

}

//MARK: UITableView Delegate & DataSource
extension MessageVC: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return messages.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let message = messages[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: message.ownerID == UserManager().currentUserID() ? "MessageTableViewCell" : "UserMessageTableViewCell") as! MessageTableViewCell
    cell.set(message)
    
    if cell.tag == 0{
        DispatchQueue.main.async {
            cell.viewSendMessage.roundCorners(corners: [.topLeft,.bottomLeft,.bottomRight], radius: 13)
        }
    }else{
        DispatchQueue.main.async {
            cell.viewReceiveMsg.roundCorners(corners: [.topRight,.bottomLeft,.bottomRight], radius: 13)
        }
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard tableView.isDragging else { return }
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
          cell.transform = CGAffineTransform.identity
        })
  }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: MessageTableViewCellDelegate Delegate
extension MessageVC: MessageTableViewCellDelegate {
  
  func messageTableViewCellUpdate() {
    tableView.beginUpdates()
    tableView.endUpdates()
  }
}
//MARK: Message Private methods
extension MessageVC {
      
    private func singleConversation(_ message: ObjectMessage)
    {
        current.singleUserConversation { (result) in
            self.arrconversation = result
            print(result.unreadCount)
            self.badge = result.unreadCount
            print(self.badge)
            self.send(message)
        }
    }
    
  private func fetchMessages() {
    SHOW_CUSTOM_LOADER()
    manager.messages(for: conversation) {[weak self] messages in
      self?.messages = messages.sorted(by: {$0.timestamp < $1.timestamp})
      self?.tableView.delegate = self
      self?.tableView.dataSource = self
      self?.tableView.reloadData()
      self?.tableView.scroll(to: .bottom, animated: true)
        HIDE_CUSTOM_LOADER()
    }
  }
  
  private func send(_ message: ObjectMessage) {
        
    manager.create(message, conversation: conversation, count: self.badge+1) {[weak self] response in
      guard let weakSelf = self else { return }
      if response == .failure {
        weakSelf.showAlert()
        return
      }
      weakSelf.conversation.timestamp = Int(Date().timeIntervalSince1970)
      switch message.contentType {
      case .none: weakSelf.conversation.lastMessage = message.message
      case .photo: weakSelf.conversation.lastMessage = "Attachment"
      case .location: weakSelf.conversation.lastMessage = "Location"
      case .offer : weakSelf.conversation.lastMessage = "Offer"
      default: break
      }
      if let currentUserID = UserManager().currentUserID() {
        weakSelf.conversation.isRead[currentUserID] = true
        //weakSelf.conversation.unreadCount = 0
      }
        self?.manager.getUnreadCount(conversation: self!.conversation) { response in
            print(response)
        }
        print(self?.user.fcm_token ?? "", self?.currentUser?.username ?? "", message.message ?? "",(self!.badge+1),(self?.user.id)!)
        let sender = PushNotificationSender()
        sender.sendPushNotification(to: self?.user.fcm_token ?? "", title: self?.currentUser?.username ?? "", body: message.message ?? "", badge: (self!.badge+1), id: (self?.user.id)!)
        ConversationManager().create(weakSelf.conversation)
        
    }
    
    
  }
  
  private func fetchUserName() {
    guard let currentUserID = UserManager().currentUserID() else { return }
    guard let userID = conversation.userIDs.filter({$0 != currentUserID}).first else { return }
    UserManager().userData(for: userID) {[weak self] user in
        let name = user?.username
        let profile = user?.profilePicLink
        
        self?.setupNavigationBarWithImage(name: name ?? "", image: profile ?? "", isBackHidden: false)
        self?.user.fcm_token = user?.fcm_token
        self?.user.id = user?.id ?? ""
        self?.user.username = name
        //self?.setNavigationTitle(name)
    }
  }
    
    func fetchProfile() {
      userManager.currentUserData {[weak self] user in
        self?.currentUser = user
      }
    }
}
class PushNotificationSender{
    func sendPushNotification(to token: String, title: String, body: String,badge : Int ,id : String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body,"sound":"default","badge":badge],
                                           "data" : ["user" : id]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(FIREBASE_SERVER_KEY)", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
