//
//  AppDelegate.swift
//  ConstrucationBuddy
//
//  Created by Jaydeep on 27/12/20.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseCore
import FirebaseFirestore
import FirebaseInstanceID
import FirebaseDynamicLinks


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var loginUserOBJ = ObjectUser()
    var arrImgUrl = [String]()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = SHARE_BASE_URL_IOS
        FirebaseApp.configure()
        
        //        UserManager().currentUserData({ (userData) in
        //            self.loginUserOBJ = userData ?? ObjectUser()
        //        })
        
        if UserManager().currentUserID() != nil {
            UserManager().userData(for: UserManager().currentUserID()!) { (objUser) in
                self.loginUserOBJ = objUser ?? ObjectUser()
                
                
                let obj = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let nav = UINavigationController(rootViewController: obj)
                nav.isNavigationBarHidden = true
                appDelegate.window?.rootViewController = nav
                
            }
            
        } else {
            let obj = mainStoryboard.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
            let nav = UINavigationController(rootViewController: obj)
            nav.isNavigationBarHidden = true
            appDelegate.window?.rootViewController = nav
        }
        
        self.registerForPushNotifications()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        return true
    }
    
    
    //MARK: - PUSH NOTIFICATION
    func registerForPushNotifications() {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                print("Permission granted: \(granted)") // 3
                let application = UIApplication.shared
                if #available(iOS 10.0, *) {
                    
                    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                    UNUserNotificationCenter.current().requestAuthorization(
                        options: authOptions,
                        completionHandler: {_, _ in })
                    
                    UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (success, error) in
                        print(error.debugDescription)
                    }
                    
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                    }
                    
                }else {
                    let type: UIUserNotificationType = [.badge, .alert, .sound]
                    let setting = UIUserNotificationSettings(types: type, categories: nil)
                    application.registerUserNotificationSettings(setting)
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                    }
                }
            }
    }
    
    
    func updateFirestorePushTokenIfNeeded() {
        let seconds = 10.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            // Put your code which should be executed with a delay here
            if let token = Messaging.messaging().fcmToken {
                guard let currentUserID = UserManager().currentUserID() else { return }
                let usersRef = Firestore.firestore().collection("Users").document(currentUserID)
                setString(value: "\(token)", key: "FCM_TOKEN")
                usersRef.setData(["fcm_token": token], merge: true)
            }
        }
        
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print(fcmToken)
        guard let currentUserID = UserManager().currentUserID() else { return }
        let usersRef = Firestore.firestore().collection("Users").document(currentUserID)
        setString(value: "\(fcmToken)", key: "FCM_TOKEN")
        usersRef.setData(["fcm_token": fcmToken], merge: true)
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        updateFirestorePushTokenIfNeeded()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        print(deviceToken)
        Messaging.messaging().apnsToken = deviceToken
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    print("Error fetching remote instance ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    setString(value: "\(result.token)", key: "FCM_TOKEN")
                    self.updateFirestorePushTokenIfNeeded()
                }
            }
        })
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        print(userInfo)
        
        let state = application.applicationState
        switch state {
        
        case .inactive:
            print("Inactive")
            
        case .background:
            print("Background")
            // update badge count here
            application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1
            
        case .active:
            print("Active")
            
        @unknown default:
            break
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        let dict = response.notification.request.content.userInfo
        print(dict)
        let badge = response.notification.request.content.badge
        print(badge ?? 0)
        //notificationTapHandle(userInfo: dict as! [String : AnyObject])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    
    //    func notificationTapHandle(userInfo : [String:AnyObject])
    //    {
    //        getAllBadgeCount()
    //        print(userInfo)
    //        self.users.id = appDelegate.objLoginModel.firebase_chat_id ?? ""
    //
    //        let pushType = userInfo["push_type"] as? String
    //        if pushType == "message"{
    //            let userid = userInfo["user"] as? String
    //            print(userid ?? "")
    //
    //            guard let currentID = UserManager().currentUserID() else { return }
    //            let VC = addStoryBoard.instantiateViewController(withIdentifier: "MessageVc") as! MessageVc
    //            if let conversation = self.conversation.filter({$0.userIDs.contains(userid ?? "")}).first {
    //              VC.conversation = conversation
    //                VC.isCheck = false
    //                manager.markAsRead(conversation)
    //              appDelegate.navigation?.pushViewController(VC, animated: true)
    //              return
    //            }
    //            let conversation = ObjectConversation()
    //            conversation.userIDs.append(contentsOf: [currentID, users.id])
    //            //conversation.isRead = [currentID: true, users.id: true]
    //            conversation.isRead[currentID] = true
    //            conversation.unreadCount = 0
    //            VC.conversation = conversation
    //            manager.markAsRead(conversation)
    //            VC.user = self.users
    //            VC.isCheck = false
    //            appDelegate.navigation?.pushViewController(VC, animated: true)
    //        }
    //
    //    }
    
    //MARK: - GET BADGE COUNT
    
    //    func getAllBadgeCount()
    //    {
    //        UserManager().currentConversations {[weak self] conversations in
    //          self?.conversation = conversations.sorted(by: {$0.timestamp > $1.timestamp})
    //            print(self!.conversation)
    //
    //            let userID = UserManager().currentUserID() ?? ""
    //
    //            var count = 0
    //            for i in self!.conversation
    //            {
    //                let isRead = i.isRead[userID] ?? true
    //                if !isRead {
    //                  count += i.unreadCount
    //                }
    //            }
    //            print("Badge Count : \(count)")
    //            UIApplication.shared.applicationIconBadgeNumber = count
    //            self?.badgeCount = count
    //        }
    //
    //    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink: dynamicLink)
            return true
        }
        return false
    }
    
    func handleIncomingDynamicLink(dynamicLink: DynamicLink) {
        guard let pathComponents = dynamicLink.url?.pathComponents else {
            return
        }
        for pathComponent in pathComponents {
            if pathComponent.contains("uid=") {
                /*let uid = pathComponent.drop(prefix: "uid=")
                Database.database().reference(withPath: "profile/" + uid).observeSingleEvent(of: .value) { (snapshot) in
                    if snapshot.exists(), var data = snapshot.value as? [String: Any] {
                        data["uid"] = snapshot.key
                        let userProfileVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                        if let tabBarVc = self.window?.rootViewController as? UITabBarController {
                            tabBarVc.selectedIndex = 1
                            if let discoveryVC = tabBarVc.viewControllers?[1] as? UINavigationController {
                                userProfileVC.selectedUser = data
                                discoveryVC.pushViewController(userProfileVC, animated: true)
                            }
                        }
                    }
                }*/
            }
        }
    }
    
 
    
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let incomingUrl = userActivity.webpageURL {
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl, completion: { (dynamicLink, _) in
                if let dynamicLink = dynamicLink, let _ = dynamicLink.url {
                    self.handleIncomingDynamicLink(dynamicLink: dynamicLink)
                }
            })
            
            if linkHandled {
                return linkHandled
            }
        }
        return false
    }
}

