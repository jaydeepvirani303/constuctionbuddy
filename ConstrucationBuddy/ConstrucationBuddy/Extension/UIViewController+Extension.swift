//
//  UIViewController+Extension.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import CoreLocation
import DropDown
import SDWebImage

extension UIViewController:NVActivityIndicatorViewable {
        
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCartAction(_ sender:UIButton){
        
    }

    //MARK:- SetupNavigation bar
    
    
    @objc func backAction() {
         self.navigationController?.popViewController(animated: true)
    }
    
    func setupNavigationBarWithCloseButton(bgColor:UIColor = APP_THEME_BLUE_COLOR,tintColor:UIColor = APP_THEME_BLUE_COLOR){
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.backgroundColor = bgColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let btnLeft = UIBarButtonItem(image: UIImage(named: "ic_close_big"), style: .plain, target: self, action: #selector(dismissVC))
        btnLeft.tintColor = tintColor
        self.navigationItem.leftBarButtonItem = btnLeft
       changeStatusBarBackgroundColor(color: bgColor)
    }

    
    func setupNavigationBarWithBackNameButton(_ name:String,bgColor:UIColor = APP_THEME_BLUE_COLOR,isBackHidden:Bool = false){
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.backgroundColor = bgColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let btnBackTitle = UIBarButtonItem(title: name, style: .plain, target: self, action: nil)
        btnBackTitle.tintColor = .white
        
        if isBackHidden {
            self.navigationItem.leftBarButtonItems = [btnBackTitle]
        } else {
            let btnBack = UIBarButtonItem(image: UIImage(named: "arrow"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
            btnBack.tintColor = .white

            self.navigationItem.leftBarButtonItems = [btnBack,btnBackTitle]
        }
        
        changeStatusBarBackgroundColor(color: bgColor)
        
    }
    
    func setupNavigationBarWithImage(name:String,image:String,bgColor:UIColor = APP_THEME_BLUE_COLOR,isBackHidden:Bool = true,isCart:Bool = false,isFilter:Bool=false){
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.backgroundColor  = bgColor
        
        let viewProfile = CustomView()
        viewProfile.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        
//        btnProfile.setImage(UIImage(named: "user_placeholder"), for: .normal)
        let imgProfile = CustomImageView()
        imgProfile.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        imgProfile.borderColor = .white
        imgProfile.borderWidth = 1
        imgProfile.clipsToBounds = true
        imgProfile.cornerRadius = imgProfile.frame.width/2
        imgProfile.contentMode = .scaleAspectFill
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: URL(string: URL_USER_PROFILE_PIC+image), placeholderImage: UIImage(named: "user_placeholder"), options: .lowPriority, context: nil)
        imgProfile.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(redirectToMyProfile))
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        viewProfile.addSubview(imgProfile)
        
        let imgLeft = UIBarButtonItem(customView: viewProfile)
        
        let lblTitle = UIBarButtonItem(title: "\(name)", style: .plain, target: self, action: nil)
        lblTitle.setTitleTextAttributes([NSAttributedString.Key.font : themeFont(size: 15, fontname: .regular)], for: .normal)
        lblTitle.tintColor = .white
        
        if isBackHidden {
            self.navigationItem.leftBarButtonItems = [lblTitle]
        } else {
            let btnBack = UIBarButtonItem(image: UIImage(named: "arrow"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
            btnBack.tintColor = .white
            self.navigationItem.leftBarButtonItems = [btnBack,lblTitle]
        }
//        if isCart {
//            var btnCart = UIBarButtonItem()
//            if isCartEmpty {
//                let button = UIButton()
//                button.setImage(UIImage(named: "ic_cart"), for: .normal)
//                button.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
//                btnCart = UIBarButtonItem(customView: button)
////                btnCart = UIBarButtonItem(image: UIImage(named: "ic_cart"), style: .plain, target: self, action: #selector(btnCartAction(_:)))
//            } else {
//                let button = UIButton()
//                button.addTarget(self, action: #selector(btnCartAction(_:)), for: .touchUpInside)
//                button.setImage(UIImage(named: "ic_cart_full"), for: .normal)
//                btnCart = UIBarButtonItem(customView: button)
////                btnCart = UIBarButtonItem(image: UIImage(named: "ic_cart_full"), style: .plain, target: self, action: #selector(btnCartAction(_:)))
//            }
////            btnCart.tintColor = .white
//            self.navigationItem.rightBarButtonItems = [imgLeft,btnCart]
//        } else {
//            self.navigationItem.rightBarButtonItems = [imgLeft]
//        }
        
        if isFilter {
            let btnClearAll = UIBarButtonItem(title: "Clear All", style: .plain, target: self, action: #selector(btnClearAllAction))
            btnClearAll.setTitleTextAttributes([NSAttributedString.Key.font:themeFont(size: 14, fontname: .regular)], for: .normal)
            btnClearAll.tintColor = .white
            self.navigationItem.rightBarButtonItems = [btnClearAll]
        }
       
        changeStatusBarBackgroundColor(color: bgColor)
    }
    
    @objc func btnClearAllAction(){
        
    }

    @objc func redirectToMyProfile(){
         
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Change StatusBar Background Color

    func changeStatusBarBackgroundColor(color:UIColor) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
    
    //MARK:- Check Valid URL
    
    func canOpenURL(_ string: String?) -> Bool {
        guard let urlString = string,
            let url = URL(string: urlString)
            else { return false }

        if !UIApplication.shared.canOpenURL(url) { return false }

        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    
    //MARK:- SetupProgressBar
    
    
    
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl,isWidth:Bool = true)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
        //        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        if isWidth {
            dropdown.width = 80
            dropdown.bottomOffset = CGPoint(x: -50, y: sender.frame.height)
        } else {
            dropdown.bottomOffset = CGPoint(x: 0, y: sender.frame.height)
            dropdown.width = sender.frame.width
        }
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    func createAttributedString(str1:String,str2:String) -> NSMutableAttributedString  {
        
        let str = str1 + " " + str2
        let interactableText = NSMutableAttributedString(string:str)
        
        let rangePolicy = (str as NSString).range(of: str2, options: .caseInsensitive)
        
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 16, fontname: .regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: APP_THEME_BLUE_COLOR, range: rangePolicy)
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 16, fontname: .bold),
                                      range: rangePolicy)
        
        return interactableText
        
    }
    
    //MARK: - StartAnimating
    
    func showLoader()
    {
        startAnimating(type: NVActivityIndicatorType.ballSpinFadeLoader)
    }
    
    func stopLoader()
    {
        self.stopAnimating()
    }

    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor(red: 35/255, green: 141/255, blue: 250/255, alpha: 1.0)
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    //MARK: - Date
    func StringToDate(Formatter : String,strDate : String) -> Date
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let convertedDate = dateformatter.date(from: strDate) else {
            let str = dateformatter.string(from: Date())
            return dateformatter.date(from: str)!
            
        }
        //        print("convertedDate - ",convertedDate)
        return convertedDate
    }
    func DateToString(Formatter : String,date : Date) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedString = dateformatter.string(from: date)
        return convertedString
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

//MARK:- UITableCell Extension

extension UITableViewCell {
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(_ label: UILabel, targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x:(labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y : locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension TimeInterval {
    var time: String {
        return String(format:"%02d:%02d", Int(self/60),  Int(ceil(truncatingRemainder(dividingBy: 60))) )
    }
}

