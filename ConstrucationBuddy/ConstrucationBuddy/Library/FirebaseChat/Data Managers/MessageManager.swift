//  MIT License

//  Created by Ghanshyam on 07/01/20.
//  Copyright © 2020 Unique Company. All rights reserved.
//

import Foundation

class MessageManager {
  
  let service = FirestoreService()
  
  func messages(for conversation: ObjectConversation, _ completion: @escaping CompletionObject<[ObjectMessage]>) {
    let reference = FirestoreService.Reference(first: .conversations, second: .messages, id: conversation.id)
    service.objectWithListener(ObjectMessage.self, reference: reference) { results in
      completion(results)
    }
  }
  
    func getUnreadCount(conversation: ObjectConversation, _ completion: @escaping CompletionObject<ObjectConversation>)
    {
//        let ref = Constants.refs.databaseChats.child(contactID).child(senderId)
//
//        ref.observe( .value, with: {(snapshot) in
//
//            let dataDic = snapshot.value as? NSDictionary
//
//            if let currentNewMsgs = dataDic!["newMessage"] as? Int
//            {
//                let newIntMsg = currentNewMsgs + 1
//                ref.setValue(newIntMsg) // This isn't right and should be updated
//            }
//
//        })
        guard let userID = UserManager().currentUserID() else { return }
        let query = FirestoreService.DataQuery(key: "userIDs", value: userID, mode: .contains)
        service.objectWithListener(ObjectConversation.self, parameter: query, reference: .init(location: .conversations)) { result in
            print(result)
        }
        
    }
    
    
    
    
    func create(_ message: ObjectMessage, conversation: ObjectConversation,count : Int, _ completion: @escaping CompletionObject<FirestoreResponse>) {
    FirestorageService().update(message, reference: .messages) { response in
      switch response {
      case .failure: completion(response)
      case .success:
        let reference = FirestoreService.Reference(first: .conversations, second: .messages, id: conversation.id)
        FirestoreService().update(message, reference: reference) { result in
          completion(result)
        }
        if let id = conversation.isRead.filter({$0.key != UserManager().currentUserID() ?? ""}).first {
          conversation.isRead[id.key] = false
          conversation.unreadCount = count
        }
        ConversationManager().create(conversation)
      }
    }
  }
}
