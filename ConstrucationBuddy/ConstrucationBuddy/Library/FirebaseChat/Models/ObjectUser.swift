

//  MIT License

//  Created by Ghanshyam on 07/01/20.
//  Copyright © 2020 Unique Company. All rights reserved.
//

import UIKit

class ObjectUser: FireStorageCodable {
    
    
    
  var id = UUID().uuidString
  var user_type: String?
  var username: String?
  var trdlicense: String?
  var email: String?
  var fcm_token: String?
  var companyname: String?
  var acctype: String?
  var profilePicLink: String?
  var profilePic: UIImage?
  var password: String?
  
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(id, forKey: .uid)
    try container.encodeIfPresent(user_type, forKey: .user_type)
    try container.encodeIfPresent(username, forKey: .username)
    try container.encodeIfPresent(trdlicense, forKey: .trdlicense)
    try container.encodeIfPresent(email, forKey: .mail)
    try container.encodeIfPresent(fcm_token, forKey: .fcm_token)
    try container.encodeIfPresent(companyname, forKey: .companyname)
    try container.encodeIfPresent(acctype, forKey: .acctype)
    try container.encodeIfPresent(profilePicLink, forKey: .profilePicLink)
  }
  
  init() {}
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decode(String.self, forKey: .uid)
    user_type = try container.decodeIfPresent(String.self, forKey: .user_type)
    username = try container.decodeIfPresent(String.self, forKey: .username)
    trdlicense = try container.decodeIfPresent(String.self, forKey: .trdlicense)
    email = try container.decodeIfPresent(String.self, forKey: .mail)
    fcm_token = try container.decodeIfPresent(String.self, forKey: .fcm_token)
    companyname = try container.decodeIfPresent(String.self, forKey: .companyname)
    acctype = try container.decodeIfPresent(String.self, forKey: .acctype)
   
    profilePicLink = try container.decodeIfPresent(String.self, forKey: .profilePicLink)
  }
}

extension ObjectUser {
  private enum CodingKeys: String, CodingKey {
    case uid
    case user_type
    case username
    case trdlicense
    case mail
    case fcm_token
    case companyname
    case acctype
    case password
    case profilePicLink
  }
}
