//  MIT License

//  Created by Ghanshyam on 07/01/20.
//  Copyright © 2020 Unique Company. All rights reserved.
//

import UIKit

class ObjectMessage: FireStorageCodable {
  
  var id = UUID().uuidString
  var message: String?
  var content: String?
  var contentType = ContentType.none
  var timestamp = Int(Date().timeIntervalSince1970)
  var ownerID: String?
  var profilePicLink: String?
  var profilePic: UIImage?
    var gameToken : String?
    var coverPic : String?
    var profile : String?
    var fullName : String?
    var itemName : String?
    var category : String?
    var desc : String?
    var location : String?
    var city : String?
    var offerToken : String?
    var offerCoverPic : String?
    var offerFullName : String?
    var offerItemName : String?
    var offerCategory : String?
    var offerDesc : String?
    var offerLocation : String?
    var offerCity : String?
    var offerProfile : String?
    var offerAccept : Int?
    
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(id, forKey: .id)
    try container.encodeIfPresent(message, forKey: .message)
    try container.encodeIfPresent(timestamp, forKey: .timestamp)
    try container.encodeIfPresent(ownerID, forKey: .ownerID)
    try container.encodeIfPresent(profilePicLink, forKey: .profilePicLink)
    try container.encodeIfPresent(contentType.rawValue, forKey: .contentType)
    try container.encodeIfPresent(content, forKey: .content)
    try container.encodeIfPresent(gameToken, forKey: .gameToken)
    try container.encodeIfPresent(coverPic, forKey: .coverPic)
    try container.encodeIfPresent(fullName, forKey: .fullName)
    try container.encodeIfPresent(itemName, forKey: .itemName)
    try container.encodeIfPresent(category, forKey: .category)
    try container.encodeIfPresent(desc, forKey: .desc)
    try container.encodeIfPresent(location, forKey: .location)
    try container.encodeIfPresent(city, forKey: .city)
    try container.encodeIfPresent(profile, forKey: .profile)
    
    try container.encodeIfPresent(offerToken, forKey: .offerToken)
    try container.encodeIfPresent(offerCoverPic, forKey: .offerCoverPic)
    try container.encodeIfPresent(offerFullName, forKey: .offerFullName)
    try container.encodeIfPresent(offerItemName, forKey: .offerItemName)
    try container.encodeIfPresent(offerCategory, forKey: .offerCategory)
    try container.encodeIfPresent(offerDesc, forKey: .offerDesc)
    try container.encodeIfPresent(offerLocation, forKey: .offerLocation)
    try container.encodeIfPresent(offerCity, forKey: .offerCity)
    try container.encodeIfPresent(offerProfile, forKey: .offerProfile)
    try container.encodeIfPresent(offerAccept, forKey: .offerAccept)
  }
  
  init() {}
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decode(String.self, forKey: .id)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    timestamp = try container.decodeIfPresent(Int.self, forKey: .timestamp) ?? Int(Date().timeIntervalSince1970)
    ownerID = try container.decodeIfPresent(String.self, forKey: .ownerID)
    profilePicLink = try container.decodeIfPresent(String.self, forKey: .profilePicLink)
    content = try container.decodeIfPresent(String.self, forKey: .content)
    if let contentTypeValue = try container.decodeIfPresent(Int.self, forKey: .contentType) {
      contentType = ContentType(rawValue: contentTypeValue) ?? ContentType.unknown
    }
    
    gameToken = try container.decodeIfPresent(String.self, forKey: .gameToken)
    coverPic = try container.decodeIfPresent(String.self, forKey: .coverPic)
    fullName = try container.decodeIfPresent(String.self, forKey: .fullName)
    itemName = try container.decodeIfPresent(String.self, forKey: .itemName)
    category = try container.decodeIfPresent(String.self, forKey: .category)
    desc = try container.decodeIfPresent(String.self, forKey: .desc)
    location = try container.decodeIfPresent(String.self, forKey: .location)
    city = try container.decodeIfPresent(String.self, forKey: .city)
    profile = try container.decodeIfPresent(String.self, forKey: .profile)
    
    offerToken = try container.decodeIfPresent(String.self, forKey: .offerToken)
    offerCoverPic = try container.decodeIfPresent(String.self, forKey: .offerCoverPic)
    offerFullName = try container.decodeIfPresent(String.self, forKey: .offerFullName)
    offerItemName = try container.decodeIfPresent(String.self, forKey: .offerItemName)
    offerCategory = try container.decodeIfPresent(String.self, forKey: .offerCategory)
    offerDesc = try container.decodeIfPresent(String.self, forKey: .offerDesc)
    offerLocation = try container.decodeIfPresent(String.self, forKey: .offerLocation)
    offerCity = try container.decodeIfPresent(String.self, forKey: .offerCity)
    offerProfile = try container.decodeIfPresent(String.self, forKey: .offerProfile)
    offerAccept = try container.decodeIfPresent(Int.self, forKey: .offerAccept)
  }
}

extension ObjectMessage {
  private enum CodingKeys: String, CodingKey {
    case id
    case message
    case timestamp
    case ownerID
    case profilePicLink
    case contentType
    case content
    case gameToken
    case profile
    case coverPic
    case fullName
    case itemName
    case category
    case desc
    case location
    case city
    case offerToken
    case offerCoverPic
    case offerFullName
    case offerItemName
    case offerCategory
    case offerDesc
    case offerLocation
    case offerCity
    case offerProfile
    case offerAccept
  }
  
  enum ContentType: Int {
    case none
    case photo
    case location
    case unknown
    case offer
  }
}
